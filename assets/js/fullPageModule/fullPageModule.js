/**
 * Created by angry_bird on 26.06.2016.
 */

require('fullpage.js');
require('../../../node_modules/fullpage.js/vendors/scrolloverflow');
require('../../../node_modules/fullpage.js/vendors/velocity.min');

module.exports = function (id) {
    id.fullpage({
        scrollOverflow: false,
        touchSensitivity: 15,
        responsiveWidth: 1025,
        anchors:['start','welcome','forCustomer','forProviders','last'],
        fitToSection:true,
        loopTop:true
    });
};