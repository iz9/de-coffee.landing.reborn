/**
 * Created by angry_bird on 12.07.2016.
 */
module.exports = function (langId) {
    var phrases = {
        "welcome_title": {
            "1": "Welcome traveller!",
            "2": "Вітаємо, мандрівниче!"
        },
        "welcome_text":{
            "1": "De.coffee is an informational service. It is designed to help customers find and acquire their favorite food and drinks. It also helps food services to find new customers and be closer to those they already have.",
            "2": "De.coffee – це інформаційний сервіс. Він створений, щоб допомагати споживачям знаходити і отримувати свої улюблені страви та напої. Цей сервіс також допомагає сервісам з надання послуг харчування знаходити нових клієнтів та залишатися поряд зі вже існуючими."
        },
        "customers_title":{
            "1": "For customers",
            "2": "Для споживачів"
        },
        "customers_tip":{
            "1": "(click here for details)",
            "2": "(натисніть сюди щоб дізнатись детальніше)"
        },
        "customers_list1":{
            "1": "- Sign in using your Facebook account",
            "2": "- Увійдіть через Фейсбук"
        },
        "customers_list2":{
            "1": "- Find the closest place to eat or drink regarding your location",
            "2": "- Знайдіть найближче місце, щоб поїсти або замовити напої, відносно вашого місцезнаходження"
        },
        "customers_list3":{
            "1": "- Preorder, using de.coffee menu",
            "2": "- Оформіть замовлення, використовуючи меню de.coffee"
        },
        "customers_list4":{
            "1": "- Come to the place where you made your order and take away when it's ready",
            "2": "- Приходьте до місця замовлення та отримуйте свої страви та напої, коли вони будуть готові"
        },
        "customers_popup_text1":{
            "1": "De.coffee is designed to help you find the closest places with food and/or drinks without having to wait for the order preparation. We work with customers through Facebook authorization. Upon completing the authorization procedure, you will be offered a map with realtime tracking of street food stalls and cafes. You can make your order online and note the time you'll come around to fetch it. After making an order you will be offered a payment method (depending on type of cooperation between de.coffee and the vendor it can be a portmone payment method or cash payment method upon arriving at the cafe or stall). Alongside making an order you will be offered an interactive chat with the vendor to clarify special wishes if you have any (like double sugars, or no sugars at all for example). The chat with the current vendor can be later accessed from you personal cabinet. After making an order you will also be given a unique code of four symbols to retrieve your order when you arrive to fetch it. The order will be ready by the time you come, so you'll just have to show the code to your vendor and leave with your favorite meal or drink. You might also have to pay of course, if the current vendor offers only cash payments. ",
            "2": "De.coffee створений, щоб допомагати знаходити найближчі місця з їжею та/або напоями без потреби чекати приготування замовлення. Ми працюємо зі споживачами через Фейсбук-авторизацію. Після проходження процедури авторизації вам буде запропоновано мапу з місцезнаходженням вуличних ларьків з їжею та/або кафе в реальному часі. Ви зможете зробити замовлення онлайн та зазначити час свого прибуття до місця приготування замовлення. Після того, як ви зробите замовлення вам буде запропоновано вибір способу оплати (в залежності від того, на яких засадах даний продавець співпрацює з de.coffee, це може бути оплата через платіжну систему Portmone або готівкою по прибутті до місця). Разом із замовленням вам буде запропоновано інтерактивний чат з продавцем, для висловлення додаткових побажань, якщо такі будуть (подвійний цукор, наприклад, або, навпаки, зовсім без цукру). Після проходження процедури замовлення ви зможете отримати доступ до чату з даним продавцем через персональний кабінет. Після оформлення замовлення вам буде надано унікальний код з чотирьох символів, щоб отримати ваше замовлення, коли ви прийдете за ним до продавця. Замовлення буде готове до вашого приходу, тому вам залишиться тільки показати код та забрати улюблену страву або напій. Можливо вам також доведеться заплатити, якщо даний продавець пропонує тільки готівкову оплату."
        },
        "customers_popup_text2":{
            "1": "If you're sure about your route, you can make multiple orders and retrieve them one by one while progressing on your way.",
            "2": "Якщо ви певні відносно свого маршруту, ви можете робити декілька замовлень і отримувати іх послідовно, просуваючись своїм шляхом."
        },
        "customers_popup_text3":{
            "1": "Please see [detailed description] of de.coffee for customers to find out  more info.",
            "2": "Будь ласка перегляньте <a href='http:google.com'>[детальний опис]</a> від de.coffee для споживачів за додатковою інформацією. З ідеюми та побажаннями пишіть нам на: de.coffee.customers@gmail.com"
        },
        "providers_title":{
            "1": "For businesses ",
            "2": "Для бізнесів"
        },
        "providers_tip":{
            "1": "(click here for details)",
            "2": "(натисніть сюди щоб дізнатись детальніше)"
        },
        "providers_list1":{
            "1": "- Register your account as a product dealer using de.coffee",
            "2": "- Зареєструйте свій аккаунт, як продавець, використовуючи de.coffee"
        },
        "providers_list2":{
            "1": "- Receive online orders using your personal working interface",
            "2": "- Отримуйте та обробляйте замовлення онлайн, використовуючи свій персональний робочий інтерфейс"
        },
        "providers_list3":{
            "1": "- Get more orders via de.coffee, get popularity among customers",
            "2": "- Отримуйте більше замовлень через de.coffee, отримуйте більше популярності серед споживачів"
        },
        "providers_popup_text1":{
            "1": "De.coffee is designed to help you find new customers and increase the popularity of your business by taking orders online. All you need to do is [register] on our website, fill in all the needed information and start working. We offer different types of cooperation for the comfort of both our clients and their customers",
            "2": "De.coffee стоверений, щоб допомагати вам в пошуці нових клієнтів та в підвищенні популярності вашого бізнесу через обробку замовлень онлайн. Все, що вам потрібно зробити, це [зареєструватися] на нашому сайті, заповнити всю потрібну інформацію та почати працювати. Ми пропонуємо різні можливості для співпраці для комфорту як наших клієнтів, так і їхніх споживачів."
        },
        "providers_popup_text2":{
            "1": "Basic cooperation requires registration online, with no additional terms apart from those stated in the [initial agreement]. It won't take long to start. Try it out right now and see for yourself how well will this work for you!",
            "2": "Базовий рівень співпраці потребує реєстрації онлайн без додактових умов, окрім тих, що зазначені в [початковій угоді]. Почати працювати за нами не займе багато часу Спробуй прямо зараз і перевір на власний досвід, як це допоможе твоїй роботі!."
        },
        "providers_popup_text3":{
            "1": "Additional features include the availability of providing online payments for your customers through Portmone and other services to improve the popularity of your stall or cafe rating in the World Wide Web. To find out more about additional features please contact us at businesses",
            "2": "Додаткові можливості співпраці включають надання опції оналайн оплати для ваших клієнтів через платіжну систему Portmone та інші сервіси, що підвищуватимуть рейтинг популярності вашого закладу у Всесвітній Мережі. За додатковою інформацією напишіть нам на de.coffee.businesses@gmail.com"
        },
        "providers_popup_text4":{
            "1": " Please see [detailed description] of de.coffee for businesses to find out  more info.",
            "2": " Будь ласка перегляньте [детальний опис] від de.coffee для бізнесів за додатковою інформацією. "
        },
        "quickstart_title":{
            "1": "Quick start",
            "2": "Швидкий старт"
        },
        "contacts_title": {
            "1": "Contact us if you have any questions!",
            "2": "Зв'яжіться з нами, якщо у вас є питання!"
        },
        "contacts_mail_customer":{
            "1": "de.coffee.customers@gmail.com – for customers",
            "2": "de.coffee.customers@gmail.com – для споживачів"
        },
        "contacts_mail_provider":{
            "1": "de.coffee.businesses@gmail.com – for businesses",
            "2": "de.coffee.businesses@gmail.com – для бізнесів"
        }
    };

    var defaultPhrase = 'def';

    this.currentLang = 1;

    this.changeLangTo = function (in_idLang) {
        in_idLang = langId;
        this.currentLang = in_idLang;
        var all_langEl = document.querySelectorAll('[data-id_phrase]');
        console.log(all_langEl);
        var el_len = all_langEl.length;
        for (var i = 0; i < el_len; i++) {
            var my_phrase = phrases[all_langEl[i].dataset.id_phrase] ? phrases[all_langEl[i].dataset.id_phrase][this.currentLang] : null;
            all_langEl[i].innerHTML = my_phrase ? my_phrase : defaultPhrase;
        }

        return true;
    }();

    /*this.getPhrase = function (in_idPhrase, in_idLang) {
        in_idLang = in_idLang || this.currentLang;
        var my_phrase = phrases[in_idPhrase] ? phrases[in_idPhrase][in_idLang] : null;

        return my_phrase ? my_phrase : defaultPhrase;
    };*/
};