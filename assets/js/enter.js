/**
 * Created by angry_bird on 26.06.2016.
 */
var $ = require('jquery');
var fullPageModule = require('./fullPageModule/fullPageModule');
var multyLanguageModule = require('./multiLanguageModule/multyLanguageModule');




$(document).ready(function () {

    var browserLangId = 1;

    function browserLangDetect() {
        var lang = navigator.language;
        if (lang == "uk" || lang == "ru") browserLangId = 2
        else browserLangId = 1
    }
    browserLangDetect();

    multyLanguageModule(browserLangId);


    var fullPageId = $('#fullpage');
    fullPageModule(fullPageId);

    $('.w-preload').addClass('fadeOut').css('z-index', -1);

    setTimeout(function () {

    }, 3000);


    if(browserLangId == 1){
        langBackgroundFunc('.c-welcomeImg', 'url(../img/de_coffe_welcome.png)');
        langBackgroundFunc('.c-miniLogo', 'url(../img/De_coffe_EN.png)');
        langBackgroundFunc('.c-logo', 'url(../img/De_coffe_EN.png)');
        langBackgroundFunc('.c-quickstartImg', 'url(../img/de_coffe_welcome.png)');
        langBackgroundHoverFunc('.c-quickstartButton__snack', 'url(../img/fancy.png)', 'url(../img/fancy_active.png)');
        langBackgroundHoverFunc('.c-quickstartButton__customer', 'url(../img/find.png)', 'url(../img/find_active.png)');
    }
    if(browserLangId == 2){
        langBackgroundFunc('.c-welcomeImg', 'url(../img/de_coffe_welcome_u.png)');
        langBackgroundFunc('.c-miniLogo', 'url(../img/De_coffe.png)');
        langBackgroundFunc('.c-logo', 'url(../img/De_coffe.png)');
        langBackgroundFunc('.c-quickstartImg', 'url(../img/de_coffe_welcome_u.png)');
        langBackgroundHoverFunc('.c-quickstartButton__snack', 'url(../img/fancy_u.png)', 'url(../img/fancy_active_u.png)');
        langBackgroundHoverFunc('.c-quickstartButton__customer', 'url(../img/find_u.png)', 'url(../img/find_active_u.png)');
    }


//Event listeners

    //language Dropdown

    var allOptions = $("#optionsList").children('li:not(.init)');
    console.log(allOptions);
    $("#optionsList").on("click", '.init', function () {
        console.log('clicked list');
        if ($('#optionsList').children()[2].style.display == 'list-item') {
            $(this).closest("#optionsList").children('li:not(.init)').slideUp();
            $('.c-language__select_triangle').removeClass('triangle-rotate');
        } else {
            $(this).closest("#optionsList").children('li:not(.init)').slideDown();
            $('.c-language__select_triangle').addClass('triangle-rotate');
        }
    });
    $("#optionsList").on("click", "li:not(.init)", function () {
        allOptions.removeClass('selected');
        $(this).addClass('selected');
        $("#optionsList").children('.init').html($(this).html());
        $('.c-language__select_triangle').removeClass('triangle-rotate');
        allOptions.slideUp();
    });


    //popups
    $('.js-providerOpenTrigger').click(function () {
        openPopup('.c-contentProvider', '.c-popupProvider', '.s-forProvider')
    });

    $('.js-providerCloseTrigger').click(function () {
        closePopup('.c-contentProvider', '.c-popupProvider', '.s-forProvider')
    });

    $('.js-manOpenTrigger').click(function () {
        openPopup('.c-contentMan', '.c-popupMan', '.s-forCustomer')
    });
    $('.js-manCloseTrigger').click(function () {
        closePopup('.c-contentMan', '.c-popupMan', '.s-forCustomer')
    });
    //popups finish

    //sections navigation
    $('.c-logo').click(function () {
       $.fn.fullpage.moveSectionDown()
    });

    $('.js-miniLogo').click(function () {
        $.fn.fullpage.moveSectionDown()
    });

    $('.js-miniLogoLast').click(function () {
        $.fn.fullpage.moveTo(2)
    });

    $('.c-welcomeImg').click(function () {
       $.fn.fullpage.moveSectionDown()
    });
    //sections navigation

    //animations
    $('.c-logo').hover(function () {
       toggleAnimation(this, 'bounce')
    });

    $('.c-quickstartButton').hover(function () {
        toggleAnimation(this, 'rubberBand')
    });
    
    $('.c-welcomeImg').hover(function () {
        toggleAnimation(this, 'pulse')
    });
    //animations finish

//Event listeners finish


//functions
    /**
     *Функция открывает попап и скрывает секцию контента,
     к которому этот попап относится + анимация
     * @param content - класс или идентификатор блока контента (String)
        вида '.contentBlockClassName' || '#contentBlockId'
     * @param popup - класс или идентификатор попапа (String)
        вида '.popupClassName' || '#popupId'
     * @param parentSection - класс или идентификатор родительской секции (String)
        вида '.parentSectionClassName' || '#parentSectionId'
     */
    function openPopup(content, popup, parentSection) {
        $(content)
            .addClass('no-opacity')
            .removeClass('slideInDown')
            .addClass('slideOutUp');
        $(popup)
            .removeClass('no-opacity')
            .css('z-index', 4)
            .removeClass('slideOutUp')
            .addClass('slideInDown');
        $(parentSection + ' .c-miniLogo').css('opacity', 0)
    }

    /**
     *Функция скрывает попап и открывает секцию контента
     к которому этот попап относится + анимация
     * @param content - класс или идентификатор блока контента (String)
     вида '.contentBlockClassName' || '#contentBlockId'
     * @param popup - класс или идентификатор попапа (String)
     вида '.popupClassName' || '#popupId'
     * @param parentSection - класс или идентификатор родительской секции (String)
     вида '.parentSectionClassName' || '#parentSectionId'
     */
    function closePopup(content, popup, parentSection) {
        $(popup)
            .addClass('no-opacity')
            .css('z-index', 0)
            .removeClass('slideInDown')
            .addClass('slideOutUp');
        $(content)
            .removeClass('slideOutUp')
            .addClass('slideInDown')
            .removeClass('no-opacity');
        $(parentSection + ' .c-miniLogo').css('opacity', 1)
    }

    /**
     *Функция задает элементу класс с анимацией при ховере
     * @param element - блок который будет анимироваться (String)
     * @param animationName - название класса анимации (String)
     */
    function toggleAnimation(element, animationName){
        $(element).toggleClass(animationName)
    }


    /**
     *
     *
     */
    function langBackgroundFunc(elem, bgi){
        $(elem).css('backgroundImage', bgi);
    }


    function langBackgroundHoverFunc(elem, bgi, hoverBgi) {
        $(elem).hover(function () {
            $(this).css('backgroundImage', hoverBgi)
        },
        function () {
            $(this).css('backgroundImage', bgi)
        })
    }

});

/*
$(window).load(function() {
    /!** код будет запущен когда страница будет полностью загружена, включая все фреймы, объекты и изображения **!/
    $('.w-preload').addClass('fadeOut').css('z-index', -1);
});*/
